
import firebase from 'firebase'
import 'firebase/firestore'

// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyAPOXf25pN6WHZ1gH6asemVsqIMAbdwGtQ",
    authDomain: "testfirebase-18a1b.firebaseapp.com",
    projectId: "testfirebase-18a1b",
    storageBucket: "testfirebase-18a1b.appspot.com",
    messagingSenderId: "821974487029",
    appId: "1:821974487029:web:5c8f8a4beb47f7b1459abc"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

let db = firebase.firestore()
let storage = firebase.storage();


export default {
    firebase, db,storage
  }