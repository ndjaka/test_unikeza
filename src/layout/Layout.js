import React from 'react';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import {makeStyles} from "@material-ui/core/styles";
import {useHistory} from 'react-router-dom'
import firebaseConfig from "../firebaseConfig";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        // maxWidth: '100%',
        marginTop: '20px',
        flexGrow: 1,

    },
    form: {
        display: 'flex',
        flexDirection: 'column',

    },

    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    addSnackbar: {
        display: 'flex',
        justifyContent: 'center',

    }
}));

const {db,firebase} = firebaseConfig;
function Layout({children}) {
  const classes = useStyles();
  const history = useHistory();


    return (
        <div>
            <AppBar position="fixed">
                <Toolbar>
                    <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                        <MenuIcon/>
                    </IconButton>
                    <Typography variant="h6" className={classes.title}>
                        Snackbars flow
                    </Typography>
                    <Button color="inherit"  onClick={()=>history.push('/fav-snack-bar') }>Mes Favoris</Button>
                    <IconButton color={'inherit'} onClick={() => firebase.auth().signOut().then(res=>history.push('/firebase-auth'))}>
                        <ExitToAppIcon/>
                    </IconButton>
                    <Typography color="inherit" variant={'button'}>{firebase.auth().currentUser?.displayName || firebase.auth().currentUser?.email}</Typography>
                </Toolbar>
            </AppBar>
            <Box my={10}/>
            {children}
        </div>
    );
}


export default Layout;