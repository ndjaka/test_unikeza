import React, { Suspense } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';


const routesConfig = [
    {
        component: <Redirect to="/" />,
        routes: [
            {
                path: "/",
                exact: true,
                component: React.lazy(() => import('./views/firebase_auth/firebase_auth'))
            },
            {
                path: "/firebase-ui-auth",
                exact: true,
                component: React.lazy(() => import('./views/firebase_ui_auth/FirebaseUiAuth'))
            },
            {
                path: "/firebase-auth",
                exact: true,
                component: React.lazy(() => import('./views/firebase_auth/firebase_auth'))
            },
            {
                path: "/firebase-register",
                exact: true,
                component: React.lazy(() => import('./views/firebase_auth/firebase_register'))
            },
            {
                path: "/firebase-forgotten-password",
                exact: true,
                component: React.lazy(() => import('./views/firebase_auth/firebase_password_forgotten'))
            },
            {
                path: "/firebase-auth-phonenumber",
                exact: true,
                component: React.lazy(() => import('./views/firebase_auth/firebase_auth_phonenumber'))
            },
            {
                path: "/home",
                exact: true,
                component: React.lazy(() => import('./views/Test/Test'))
            },
            {
                path: "/snack-bar",
                exact: true,
                component: React.lazy(() => import('./views/Test/bar'))
            },
            {
                path: "/fav-snack-bar",
                exact: true,
                component: React.lazy(() => import('./views/Test/favorite_bar'))
            },
            {
                path: "/details-snack/:id",
                exact: true,
                component: React.lazy(() => import('./views/Test/details_snack'))
            }
        ]
    }
];


function renderRoutes(routes) {
    return (
        <Router>
            <Suspense fallback={<div>Chargement...</div>}>
                <Switch>
                    {
                        routes.map((route, i) => <Route
                            key={i}
                            path={route.path}
                            exact={route.exact}
                            render={(props) => {
                                const Component = route.component;
                                return (
                                    (
                                        <div>
                                            {
                                                route.routes ? (
                                                    renderRoutes(route.routes)
                                                ) : (
                                                    <Component {...props} />
                                                )
                                            }
                                        </div>

                                    )
                                )
                            }
                            }
                        />
                        )
                    }


                </Switch>
            </Suspense>
        </Router>
    );
}
export default function Routes() {
    return renderRoutes(routesConfig);
}
