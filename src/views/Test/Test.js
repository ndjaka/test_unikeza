import React, { useEffect } from 'react';
import {
  Container,
  IconButton,
  Box,
  TextField,
  Divider,
  Button,
  Grid,
  CircularProgress,
  LinearProgress,
  ListItemSecondaryAction,
  Link
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import EditIcon from '@material-ui/icons/Edit';
import Avatar from '@material-ui/core/Avatar';
import '../../index';
import { useHistory } from 'react-router-dom';
import firebaseConfig from "../../firebaseConfig";

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: '100%',
    backgroundColor: theme.palette.background.paper,
    marginTop: '20px'
  },
  form: {
    display: 'flex',
    flexDirection: 'column',

  },
}));

const initialState = {
  fullname: '',
  email: '',
  phone: '',
  id: ''
}
const { firebase,db } = firebaseConfig;
function App() {
  const classes = useStyles();
  const [loading, setLoading] = React.useState(false);
  const [errorListing, setErrorListing] = React.useState(false);
  const [loadingListUsers, setLoadingListUsers] = React.useState(false);
  const [formState, setFormState] = React.useState(initialState);
  const [users, setUsers] = React.useState([]);
  const history = useHistory();
  // ajout des utilisateurs
  const addUsers = (e) => {
    e.preventDefault();
    setLoading(true)
      db.collection('users').add(formState)
      .then(documentReference => {
        console.log('document reference ID', documentReference.id);
        setFormState(initialState)
        setUsers(prev => ([formState, ...prev]))
      })
      .catch(error => {
        console.log(error.message)
        setErrorListing(true);
      }).finally(() => setLoading(false))

  }

  //Mise a jour des utilisateurs

  const updateUsers = (e) => {
    e.preventDefault();

    setLoading(true)
    db.collection('users').doc(formState.id)
      .update({
        fullname: formState.fullname,
        email: formState.email,
        phone: formState.phone,
      })
      .then(documentReference => {
        setUsers(users.map(user => user.id === formState.id ? formState : user))
      })
      .catch(error => {
        console.log(error.message)
      }).finally(() => setLoading(false))

  }



  // listes des utilisateurs 
  const getUsers = () => {
    setLoadingListUsers(true)
   db.collection('users').get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          console.log('object', doc)

          setUsers(prev => ([...prev, {
            id: doc.id,
            ...doc.data()

          }]))
        })
      })
      .catch(err => {
        console.log(err.message)

      }).finally(() => setLoadingListUsers(false))

  }


  useEffect(() => {
    setUsers([])
    getUsers()
  }, []);


  return (

    <Container maxWidth={'lg'} >
      <Grid container spacing={4} >
        <Grid item lg={4} md={4} sm={12} xs={12}>
          <form noValidate autoComplete="off" className={classes.form} >
            <TextField
              id="standard-basic"
              label="Nom complet"
              value={formState.fullname}
              onChange={(e) => setFormState(form => ({
                ...formState,
                fullname: e.target.value
              }))}
            />
            <TextField id="standard-basic" label="Adresse email"
              value={formState.email}
              onChange={(e) => setFormState(form => ({
                ...formState,
                email: e.target.value
              }))}
            />
            <TextField id="standard-basic" label="Téléphone"
              value={formState.phone}
              onChange={(e) => setFormState(form => ({
                ...formState,
                phone: e.target.value
              }))} />
            <Box mt={'20px'} justifyContent={'center'} display={'flex'} justifyContent={'space-between'}>
              <Button

                variant={'contained'}
                color={'secondary'}
                onClick={() => setFormState(initialState)

                }>

                Annuler
              </Button>
              <Button disabled={loading} variant={'contained'} color={'primary'} onClick={formState.id !== '' ? updateUsers : addUsers}>
                {loading && <CircularProgress color={'secondary'} size={21} />}
                {formState.id !== '' ? 'Modifier' : 'Ajouter'}
              </Button>
           </Box>

          </form>
          <Box onClick={() => firebase.auth().signOut().then(res=>history.push('/firebase-auth'))}
               mt={'20px'} justifyContent={'center'} display={'flex'}>
            <Link >Deconnexion</Link>
          </Box>
        </Grid>
        <Grid item lg={8} md={8} sm={12} xs={12}>

          {
            !errorListing && users.length === 0 && loadingListUsers ? <LinearProgress /> :
              <List>
                {
                  users.map((user, id) => (
                    <div key={user.id}>
                      <Divider variant="inset" component="li" />
                      <ListItem >
                        <ListItemAvatar>
                          <Avatar />
                        </ListItemAvatar>
                        <ListItemText primary={user.fullname} secondary={user.email} />
                        <ListItemSecondaryAction>
                          <IconButton onClick={() => setFormState(user)} edge="end" aria-label="delete">
                            <EditIcon />
                          </IconButton>
                        </ListItemSecondaryAction>
                      </ListItem>
                    </div>
                  )
                  )
                }
              </List>

          }
          {
            errorListing && <Box mt={'40px'} display={'flex'} justifyContent={'center'}  >
              <Button color={'primary'} variant={'contained'} onClick={getUsers}>Ressayer</Button>
            </Box>

          }

        </Grid>



      </Grid>
    </Container >


  );
}


export default App;




