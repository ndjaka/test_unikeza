import {makeStyles} from '@material-ui/core/styles';
import firebaseConfig from "../../firebaseConfig";
import {Container, Grid} from "@material-ui/core";
import React, {useEffect} from "react";
import {AddDialog, Snackbar, SnackbarList} from "./components";
import Box from "@material-ui/core/Box";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import LinearProgress from "@material-ui/core/LinearProgress";
import { useHistory } from 'react-router-dom';
import Layout from "../../layout/Layout";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        // maxWidth: '100%',
        marginTop: '20px',
        flexGrow: 1,

    },
    form: {
        display: 'flex',
        flexDirection: 'column',

    },

    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    addSnackbar: {
        display: 'flex',
        justifyContent: 'center',

    }
}));

const {firebase, db} = firebaseConfig;

function Snackbars() {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [limit, setLimit] = React.useState(6);
    const [loadingSnack, setLoadingSnack] = React.useState(false);
    const [snackbars, setSnackbar] = React.useState([]);
    const history = useHistory();

    const getSnacks = () => {
        setLoadingSnack(true)
        db.collection('snackbars').limit(limit).get()
            .then(querySnapshot => {

                setSnackbar(querySnapshot.docs.map(doc => ({
                    id: doc.id,
                    ...doc.data()

                })))
            })
            .catch(err => {
                console.log(err.message)

            }).finally(() => setLoadingSnack(false))

    }


    useEffect(() => {
        getSnacks();
    }, [limit]);

    React.useEffect(()=>{
        db.collection('snackbars').onSnapshot(snapshot => {
            snapshot.docChanges().forEach(change => {
                // if (change.type === "added") {
                //     setSnackbar([{id: change.doc.id, ...change.doc.data()}, ...snackbars,])
                // }
                if (change.type === "modified") {
                    //  console.log("updated tutorial: ", change.doc.data(),'id',change.doc.id);
                    setSnackbar(snackbars.map(snack => snack.id === change.doc.id ? {...snack, ...change.doc.data()} : snack));
                }

            })
        });
    })


    return (
        <>
            {
                open && <AddDialog fetchData={() => getSnacks()} open={open} setOpen={() => setOpen(false)}/>
            }
            <Layout>
                <Box className={classes.addSnackbar}>
                    <IconButton aria-label="add to favorites" onClick={() => setOpen(true)}>
                        <AddCircleOutlineIcon fontSize={'large'}/>
                    </IconButton>
                </Box>
                <Container maxWidth={'md'} className={classes.root}>
                    {snackbars.length === 0 && loadingSnack && <LinearProgress/>}
                    <SnackbarList snackbars={snackbars}/>
                </Container>
                <Box onClick={()=>setLimit(limit=>limit+6)} mt={'8px'} display={'flex'} justifyContent={'center'}><Button>Voir plus</Button></Box>

            </Layout>
        </>
    );
}

export default Snackbars;




