import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Avatar from "@material-ui/core/Avatar";

import {makeStyles} from '@material-ui/core/styles';
import {LinearProgress, Typography} from "@material-ui/core";
import Box from "@material-ui/core/Box";
import firebaseConfig from "../../../firebaseConfig";
import CircularProgress from "@material-ui/core/CircularProgress";


const useStyles = makeStyles((theme) => ({

    small: {
        width: theme.spacing(3),
        height: theme.spacing(3),
    },
    large: {
        marginTop: 5,
        width: theme.spacing(20),
        height: theme.spacing(20),
        cursor: 'pointer'
    },
    input: {
        display: 'none',
    },
    images: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column'
    }
}));

const initialState = {
    name: '',
    phoneNumber: '',
    quater: '',
    specifities: ''
}

const {db,storage} = firebaseConfig;

export default function AddDialog({open, setOpen, fetchData}) {
    const classes = useStyles();
    const [img, setImg] = React.useState();
    const [file, setFile] = React.useState();
    const [progress, setProgress] = React.useState(0);
    const [error, setError] = React.useState(null);
    const [formState, setFormState] = React.useState(initialState);
    const [loading, setLoading] = React.useState(false);
    const storageRef = storage.ref(file?.name);
    const handleClose = () => {
        setOpen(false);
        fetchData();
    };

    const getBase64FromUrl = async (_file) => {
        return new Promise((resolve) => {
            const reader = new FileReader();
            reader.readAsDataURL(_file);
            reader.onloadend = () => {
                const base64data = reader.result;
                resolve(base64data);
            }
        });
    }

    const handleAddSnackbar = (url) => {
        setLoading(true)
        let doc;
        if(url === undefined) {
            doc= db.collection('snackbars').add({...formState})
        }else{
            doc=  db.collection('snackbars').add({...formState,img:url})
         }
         doc.then(documentReference => {
                console.log('document reference ID', documentReference.id);
                setFormState(initialState);
                handleClose()

            })
            .catch(error => {
                console.log(error.message)
            }).finally(() => setLoading(false))
    }

    const handleStartProcessing =  () => {
        if(!file){
            handleAddSnackbar()
        }else{
            storageRef.put(file).on('state_changed', (snap) => {
                let percentage = (snap.bytesTransferred / snap.totalBytes) * 100;
                setProgress(percentage);
            }, (err) => {
                setError(err);
            }, async () => {
                const url = await storageRef.getDownloadURL();
                handleAddSnackbar(url)
            })
        }


    }


    return (
        <div>
            <Dialog maxWidth={'sm'} open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Ajouter le snack</DialogTitle>
                <DialogContent>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        label="Nom"
                        onChange={e => setFormState({...formState, name: e.target.value})}
                        type="text"
                        fullWidth
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        onChange={e => setFormState({...formState, phoneNumber: e.target.value})}
                        label="Numéro"
                        type="text"
                        fullWidth
                    />
                    <TextField
                        autoFocus
                        onChange={e => setFormState({...formState, quater: e.target.value})}

                        margin="dense"
                        id="name"
                        label="Quartier"
                        type="text"
                        fullWidth
                    />
                    <TextField
                        autoFocus
                        onChange={e => setFormState({...formState, specifities: e.target.value})}
                        multiline
                        rows={4}
                        margin="dense"
                        id="name"
                        label="Specifités"
                        type="text"
                        fullWidth
                    />
                    <Box className={classes.images}>
                        <input
                            onChange={event => {
                                setFile(event.target.files[0]);
                                getBase64FromUrl(event.target.files[0]).then(res => setImg(res));
                            }}
                            accept="image/*"
                            className={classes.input}
                            id="icon-button-file"
                            type="file"
                        />
                        <label htmlFor="icon-button-file">
                            <Avatar src={img} variant={'square'} alt="Remy Sharp" className={classes.large}/>
                        </label>
                        <Typography>cliquer pour ajouter l'image de votre snack</Typography>
                    </Box>
                    {progress > 0 && <LinearProgress value={progress} variant={'determinate'}/>}

                </DialogContent>
                <DialogActions>
                    <Button variant={'contained'} onClick={handleClose} color="secondary">
                        Fermer
                    </Button>
                    <Button disabled={loading} variant={'contained'}
                            onClick={handleStartProcessing}
                            color="primary">
                        {loading && <CircularProgress color={'secondary'} size={21}/>} Enregistrer
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
