import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import FavoriteIcon from '@material-ui/icons/Favorite';
import Typography from '@material-ui/core/Typography';
import RoomIcon from '@material-ui/icons/Room';
import Box from "@material-ui/core/Box";
import Rating from "@material-ui/lab/Rating";
import IconButton from "@material-ui/core/IconButton";
import firebaseConfig from "../../../firebaseConfig";
import LinearProgress from "@material-ui/core/LinearProgress";
import {useHistory} from "react-router-dom";
const { firebase,db } = firebaseConfig;

const useStyles = makeStyles({
    root: {
        maxWidth: 300,
    },
    media: {
        height: 140,
    },
    content:{
        wordBreak:'break-all'
    }
});

export default function Snackbar(props) {
    const classes = useStyles();
    const {name,ratings,quater,specifities,is_favorite,img , onAddTofavorite ,id }=props;
    const history = useHistory();

    return (
        <Card className={classes.root}>
            <CardActionArea
                onClick={()=>history.push(`/details-snack/${id}`)}>
                <CardMedia
                    className={classes.media}
                    image={img}
                    title="Le click plus"
                />
                <CardContent className={classes.content}>
                    <Box display={'flex'} justifyContent={'space-between'}>
                        <Typography gutterBottom variant="h6" component="h2">
                            {name}
                        </Typography>
                        <Box mt={'3px'}>
                            <Rating name="read-only" value={ratings} readOnly size="small" />
                        </Box>
                    </Box>


                    <Box display={'flex'} ml={'-2px'} mb={'6px'}>
                        <RoomIcon/>
                        <Box>
                            <Typography variant="subtitle1" color="textSecondary" component="p">
                                {quater}
                            </Typography>
                        </Box>
                    </Box>
                    <Typography variant="body2" color="textSecondary" component="p">
                        {specifities}
                    </Typography>
                </CardContent>
            </CardActionArea>
            <CardActions disableSpacing>
                <IconButton aria-label="add to favorites" onClick={onAddTofavorite}>
                    <FavoriteIcon  color={is_favorite ? 'secondary':'inherit'}  />
                </IconButton>

            </CardActions>
        </Card>
    );
}
