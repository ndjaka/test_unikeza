import React from 'react';
import {Container, Grid} from "@material-ui/core";
import {Snackbar} from "./index";
import firebaseConfig from "../../../firebaseConfig";
import {useHistory} from 'react-router-dom';
const {firebase, db} = firebaseConfig;

function SnackbarList({ snackbars }) {
    const [loadingFav, setLoadingFav] = React.useState(false);
    const snackbarRef = db.collection('snackbars');
    const addTofavorites = (id, is_favorite) => {
        setLoadingFav(true);
        snackbarRef.doc(id).update({
            is_favorite: is_favorite
        }).then(querySnapshot => {
        })
            .catch(err => {
                console.log(err.message)

            }).finally(() => setLoadingFav(false))
    }
    return (
        <Grid container spacing={2} >
            {snackbars.map((item, id) => (
                    <Grid  key={id} item lg={4} md={4} sm={6}>
                         <Snackbar
                             id={item.id}
                            loading={loadingFav}
                            onAddTofavorite={() => addTofavorites(item.id, !item.is_favorite)}
                            name={item.name}
                            ratings={item.ratings}
                            quater={item.quater}
                            specifities={item.specifities}
                            is_favorite={item.is_favorite}
                            img={item.img || 'https://via.placeholder.com/140'}
                        />
                    </Grid>
                )
            )}
        </Grid>
    );
}


export default SnackbarList;