import React, {useEffect} from 'react';
import Layout from "../../layout/Layout";
import {makeStyles} from "@material-ui/core/styles";
import {Button, Container} from "@material-ui/core";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Rating from "@material-ui/lab/Rating";
import RoomIcon from "@material-ui/icons/Room";
import {Edit, Phone} from "@material-ui/icons";
import IconButton from "@material-ui/core/IconButton";
import FavoriteIcon from "@material-ui/icons/Favorite";
import DialogActions from "@material-ui/core/DialogActions";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import {useParams} from 'react-router-dom';
import firebaseConfig from "../../firebaseConfig";
import CircularProgress from "@material-ui/core/CircularProgress";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        // maxWidth: '100%',
        position:'relative',
        marginTop: '20px',
        wordBreak:'break-all'

    },
    form: {
        display: 'flex',
        flexDirection: 'column',

    },

    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    addSnackbar: {
        display: 'flex',
        justifyContent: 'center',

    },
    img: {
        width: '100%',
        height: '300px',
        objectFit: 'cover'
    }
}));

const {db} = firebaseConfig;

function Detailsbar() {
    const classes = useStyles()
    const [open, setOpen] = React.useState(false);
    const [data, setData] = React.useState({});
    let {id} = useParams();
    const snackbarRef = db.collection('snackbars').doc(id);
    const doc = db.collection('snackbars').doc(id);

    const getSnack = () => {
        snackbarRef.get()
            .then(querySnapshot => setData(querySnapshot.data()))
            .catch(err => {
                console.log(err.message)

            })

    }
    useEffect(() => {
        getSnack();
    }, [id]);

    useEffect(() => {
       doc.onSnapshot(docSnapshot => {
           setData(docSnapshot.data());
            console.log(`Received doc snapshot: `,docSnapshot.data());
            // ...
        }, err => {
            console.log(`Encountered error: ${err}`);
        });
    }, []);

    return (
        <Layout>
            {open && <RatingDialog open={open} close={() => setOpen(false)}/>}
            <Container maxWidth={'md'} className={classes.root}>
                <img src={data?.img || 'https://via.placeholder.com/300'} alt="" className={classes.img}/>
                <Box my={'3px'}/>
                <Box display={'flex'} justifyContent={'space-between'}>
                    <Typography gutterBottom variant="h6" component="h2">
                        {data.name}
                    </Typography>
                    <Box mt={'3px'}>
                        <FavoriteIcon color={data.is_favorite ? 'secondary' : 'inherit'}/>
                    </Box>
                </Box>


                <Box display={'flex'} ml={'-2px'} mb={'6px'}>
                    <RoomIcon fontSize={'small'}/>
                    <Box>
                        <Typography variant="subtitle1" color="textSecondary" component="p">
                            {data.quater}
                        </Typography>
                    </Box>
                </Box>
                <Box display={'flex'} ml={'-2px'} mb={'6px'}>
                    <Phone fontSize={'small'}/>
                    <Box>
                        <Typography variant="subtitle1" color="textSecondary" component="p">
                            {data?.phoneNumber}
                        </Typography>
                    </Box>
                </Box>

                <Box>
                    <Rating name="read-only" value={parseInt(data.ratings)} readOnly size="small"/>
                    <IconButton style={{marginTop: '-10px'}} onClick={() => setOpen(true)}>
                        <Edit style={{fontSize: 15}}/>
                    </IconButton>
                </Box>
                <Typography variant="body2" color="textSecondary" >
                    {data.specifities}
                </Typography>
            </Container>

        </Layout>
    );
}

const RatingDialog = ({open, close}) => {
    const [value, setValue] = React.useState();
    const snackbarRef = db.collection('snackbars');
    const [loadingFav, setLoadingFav] = React.useState(false);
    let {id} = useParams();

    const addRatings = () => {
        setLoadingFav(true);
        snackbarRef.doc(id).update({
            ratings: value
        }).then(querySnapshot => {
            close()
        })
            .catch(err => {
                console.log(err.message)

            }).finally(() => setLoadingFav(false))
    }

    return (
        <Dialog open={open} onClose={close}>
            <DialogTitle id="alert-dialog-title">Noter le bar</DialogTitle>
            <DialogContent>
                <Box display={'flex'} my={'6px'} justifyContent={'center'}>
                    <Rating value={value}
                            onChange={(event, newValue) => {
                                setValue(newValue);
                            }} size="large"/>
                </Box>
            </DialogContent>
            <DialogActions>
                <Button onClick={close} variant={'contained'} color={'secondary'}>Fermer</Button>

                <Button disabled={loadingFav} onClick={addRatings} variant={'contained'} color={'primary'}>
                    {loadingFav && <CircularProgress size={18} color={'secondary'}/>}
                    Enregistrer
                </Button>

            </DialogActions>
        </Dialog>
    )
}

export default Detailsbar;