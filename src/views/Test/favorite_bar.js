import React, {useEffect} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {Container} from "@material-ui/core";
import LinearProgress from "@material-ui/core/LinearProgress";
import {SnackbarList} from "./components";
import Box from "@material-ui/core/Box";
import firebaseConfig from "../../firebaseConfig";
import Typography from "@material-ui/core/Typography";
import Layout from "../../layout/Layout";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        // maxWidth: '100%',
        marginTop: '20px',
        flexGrow: 1,

    },
    form: {
        display: 'flex',
        flexDirection: 'column',

    },

    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    addSnackbar: {
        display: 'flex',
        justifyContent: 'center',

    }
}));

const {firebase, db} = firebaseConfig;

function FavoviteBar() {
    const [loadingFavSnack, setLoadingFavSnack] = React.useState(false);
    const [snackbars, setSnackbar] = React.useState([]);
    const classes = useStyles();
    const snackbarRef = db.collection('snackbars').where('is_favorite', '==', true);
    const getSnacks = () => {
        setLoadingFavSnack(true)
        snackbarRef.get().then(querySnapshot => {
            setSnackbar(querySnapshot.docs.map(doc => ({
                id: doc.id,
                ...doc.data()

            })))
        })
            .catch(err => {
                console.log(err.message)

            }).finally(() => setLoadingFavSnack(false))

    }

   useEffect(()=>{
        snackbarRef.onSnapshot(snapshot => {
            snapshot.docChanges().forEach(change => {
                // if (change.type === "added") {
                //     setSnackbar([{id: change.doc.id, ...change.doc.data()}, ...snackbars,])
                // }
                if (change.type === "modified") {
                    //  console.log("updated tutorial: ", change.doc.data(),'id',change.doc.id);
                    setSnackbar(snackbars.map(snack => snack.id === change.doc.id ? {...snack, ...change.doc.data()} : snack));
                }

            })
        });
    })

    useEffect(() => {
        getSnacks();
    }, []);

    return (
        <Container maxWidth={'md'} className={classes.root}>
            <Layout>
                <Box my={'3px'}> <Typography variant={'h4'}>Mes favoris</Typography></Box>
                {snackbars.length === 0 && loadingFavSnack && <LinearProgress/>}
                <SnackbarList snackbars={snackbars}/>
            </Layout>
        </Container>
    );
}


export default FavoviteBar;