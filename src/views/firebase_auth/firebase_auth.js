import React, { useState } from 'react';
import { Container, Box, TextField, Divider, Button, Grid, CircularProgress, LinearProgress, makeStyles, Link, } from '@material-ui/core';
import styles from './app.module.css'; // This uses CSS modules.
import clsx from 'clsx';
import firebaseConfig from '../../firebaseConfig';
import { Alert } from '@material-ui/lab';
import { useHistory } from 'react-router-dom';
import {PhoneIphone} from "@material-ui/icons";


const useStyles = makeStyles((theme) => ({
    root: {

        width: '25%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        maxWidth: '100%',
        height: '100vh',
        backgroundColor: 'transparent',
        marginTop: '20px'

    },
    form: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%',


    },
    link: {
        cursor: 'pointer'
    },
    logo: { fontfamily: "'Amaranth', sans-serif", fontSize: "200%" },
    logoIcon: {
        top: "4px",
        fontSize: "32px",
        marginRight: "-2px",
        position: "relative",
        color: 'black'


    },
    caption: {
        margin: "20px 0 40px 0",
        fontFamily: "'Amaranth', sans-serif",
        fontSize: "18px",
        opacity: 0.8,
        color: 'black'
    }
}));

const initialState = {
    email: '',
    pwd: '',
}
const { firebase } = firebaseConfig;

function App() {
    const classes = useStyles();
    const [formState, setFormState] = React.useState(initialState)
    const [loading, setLoading] = React.useState(false)
    const [alert, setAlert] = React.useState();
    const [isSignedIn, setIsSignedIn] = React.useState(false);
    const history = useHistory();

    const handleSignIn = () => {
        setLoading(true)
        firebase.auth().signInWithEmailAndPassword(formState.email, formState.pwd)
            .then((userCredential) => {
                // Signed in
                var user = userCredential.user;
                if (!user.emailVerified) {
                    setAlert({
                        severity: 'error',
                        message: 'Votre compte n\'a pas encore été activer'
                    })
                }
            })
            .catch((error) => {
                var errorCode = error.code;
                var errorMessage = error.message;
                setAlert({
                    severity: 'error',
                    message: errorMessage
                })

            }).finally(() => setLoading(false));
    }

    const authWithGoogle = () => {
        var provider = new firebase.auth.GoogleAuthProvider();
        provider.addScope('https://www.googleapis.com/auth/contacts.readonly');
        firebase.auth().useDeviceLanguage();
        firebase.auth().signInWithPopup(provider)
            .then((result) => {

                var credential = result.credential;
                var token = credential.accessToken;
                var user = result.user;

            }).catch((error) => {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                // The email of the user's account used.
                var email = error.email;
                // The firebase.auth.AuthCredential type that was used.
                var credential = error.credential;

                setAlert({
                    severity: 'error',
                    message: errorMessage
                })
            });

    }

    const authWithFacebook = () => {
        var provider = new firebase.auth.FacebookAuthProvider();
        firebase
            .auth()
            .signInWithPopup(provider)
            .then((result) => {
                /** @type {firebase.auth.OAuthCredential} */
                var credential = result.credential;

                // The signed-in user info.
                var user = result.user;

                // This gives you a Facebook Access Token. You can use it to access the Facebook API.
                var accessToken = credential.accessToken;

                // ...
            })
            .catch((error) => {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                // The email of the user's account used.
                var email = error.email;
                // The firebase.auth.AuthCredential type that was used.
                var credential = error.credential;

                setAlert({
                    severity: 'error',
                    message: errorMessage
                })
            });

    }

    React.useEffect(() => {
        const unregisterAuthObserver = firebase.auth().onAuthStateChanged(user => {
            if (user) {
                history.push('/snack-bar')
                setIsSignedIn(user.emailVerified);
            }
        });
        return () => unregisterAuthObserver(); // Make sure we un-register Firebase observers when the component unmounts.
    }, []);


    return (

        <Container maxWidth={'sm'} className={clsx(classes.root)}  >
            {alert && <Alert severity={alert.severity}>{alert.message}</Alert>}
            <div className={classes.logo}>
            <i className={classes.logoIcon + ' material-icons'}></i> My Snack Flow
        </div>
            <div className={classes.caption}>Tous vos snack en un seul endroit</div>
            <form noValidate autoComplete="off" className={classes.form} >
                    <TextField
                        fullWidth
                        id="standard-basic"
                        label="Email"
                        value={formState.email}
                        onChange={(e) => setFormState(form => ({
                            ...formState,
                            email: e.target.value
                        }))}
                    />
                    <TextField
                        fullWidth
                        type={'password'}
                        id="standard-basic" label="Mot de passe"
                        value={formState.pwd}
                        onChange={(e) => setFormState(form => ({
                            ...formState,
                            pwd: e.target.value
                        }))}
                    />
                <Box mt={'5px'} display={'flex'} justifyContent={'flex-end'}  className={classes.link}><Link onClick={() => history.push('/firebase-forgotten-password')} > Mot de passe oublié ? </Link></Box>


                <Box mt={'20px'} justifyContent={'center'} display={'flex'} flexDirection={'column'}>

                        <Button disabled={loading} variant={'contained'} color={'primary'} onClick={handleSignIn} >
                            {loading && <CircularProgress color={'secondary'} size={21} />}
                            enregistrer
                        </Button>
                        <Box my={'6px'} display={'flex'} justifyContent={'center'}>Se connecter avec </Box>
                        <Button onClick={authWithGoogle} variant={'contained'} color={'secondary'}>Google</Button>
                        <Box my={'3px'} />
                        <Button onClick={authWithFacebook} variant={'contained'} color={'primary'}>FaceBook</Button>
                        <Box my={'3px'} />
                         <Button startIcon={<PhoneIphone/>} variant={'contained'} color={'primary'} onClick={() => history.push('/firebase-auth-phonenumber')}> Son numéro</Button>
                    </Box>
                    <Box mt={'5px'} display={'flex'} justifyContent={'flex-end'} className={classes.link}><Link onClick={() => history.push('/firebase-register')} > Pas encore de compte ? S'inscrire</Link></Box>
                    <Box mt={'10px'} display={'flex'} justifyContent={'center'} className={classes.link}><Link onClick={() => history.push('/firebase-ui-auth')} >Essayez Firebase Auth UI</Link></Box>

                </form>

            {
                isSignedIn && <Box justifyContent={'center'} component={'span'}>
                    {`${firebase.auth().currentUser.email}  You are now signed In!`} <Link onClick={() => firebase.auth().signOut()} >Deconnexion</Link>
                </Box>
            }


        </Container >


    );
}


export default App;




