import React, {useEffect, useState} from 'react';
import { Container, Box, TextField, Divider, Button, Grid, CircularProgress, LinearProgress, makeStyles, Link, } from '@material-ui/core';
import styles from './app.module.css'; // This uses CSS modules.
import clsx from 'clsx';
import firebaseConfig from '../../firebaseConfig';
import { Alert } from '@material-ui/lab';
import { useHistory } from 'react-router-dom';


const useStyles = makeStyles((theme) => ({
    root: {
        width: '25%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        maxWidth: '100%',
        height: '100vh',
        backgroundColor:'transparent',
        marginTop: '20px'
    },
    form: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%',


    },
    logo: { fontfamily: "'Amaranth', sans-serif", fontSize: "200%" },
    logoIcon: {
        top: "4px",
        fontSize: "32px",
        marginRight: "-2px",
        position: "relative",
        color: 'black'


    },
    caption: {
        margin: "20px 0 40px 0",
        fontFamily: "'Amaranth', sans-serif",
        fontSize: "18px",
        opacity: 0.8,
        color: 'black'
    }
}));


const { firebase } = firebaseConfig;

function App() {
    const classes = useStyles();
    const [phoneNumber, setPhoneNumber] = React.useState('')
    const [code, setCode] = React.useState('');
    const [showCodeField, setCodeCodeField] = React.useState(false)
    const [loading, setLoading] = React.useState(false)
    const [alert, setAlert] = React.useState();
    const history = useHistory();


    const handleRegister = event => {
        event.preventDefault();
        setLoading(true);
        window.appVerifier = new firebase.auth.RecaptchaVerifier(
            "recaptcha-container",
            {
                size: "normal",
                'callback': (response) => {
                   document.getElementById('recaptcha-container').style.display = 'none'
                },
                'expired-callback': () => {
                    // Response expired. Ask user to solve reCAPTCHA again.
                    // ...
                }
            }
        );
        const appVerifier = window.appVerifier;
        firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
            .then((confirmationResult) => {
                setCodeCodeField(true);
                setAlert({
                    severity:"info",
                    message:"Consultez le code que vous avez recu par message pour continuez"
                })
                window.confirmationResult = confirmationResult;
                setCodeCodeField(true)
            }).catch((error) => {
            setAlert({
                severity:"error",
                message:error.message
            })

        }).finally(()=>     setLoading(false));


    }
  const  onVerifyCodeSubmit = event => {
      event.preventDefault();
      setLoading(true);

      window.confirmationResult
          .confirm(code)
          .then(function (result) {
              var user = result.user;
              user.getIdToken().then(idToken => {
                  console.log(idToken);
              });
              history.push('/home')
          })
          .catch(function (error) {
              // User couldn't sign in (bad verification code?)
              console.error("Error while checking the verification code", error);
              setAlert({
                  severity: "error",
                  message: error.message
              })


          }).finally(() => setLoading(false));
  }




    return (

        <Container maxWidth={'sm'} className={clsx(classes.root)}  >
            {alert && <Alert severity={alert.severity}>{alert.message}</Alert>}
            <div className={classes.logo}>
                <i className={classes.logoIcon + ' material-icons'}></i> My Snack Flow
            </div>
            <div className={classes.caption}>Tous vos snack en un seul endroit</div>
            <form noValidate autoComplete="off" className={classes.form} >

                {!showCodeField &&(
                    <>
                    <Box component={'div'} my={'3px'} align={'center'} > Saissisez un numéro avec l'indicatif +237 exemple :  (+237691966876)</Box>
                    <TextField
                    fullWidth
                    id="standard-basic"
                    label="Numéro de téléphone"
                    value={phoneNumber}
                    onChange={(e) => setPhoneNumber(e.target.value)}
                />
                </>)}
                {<Box component={'div'} my={'3px'} id="recaptcha-container"/> }
                {!!showCodeField && <TextField
                    fullWidth
                    placeholder={'Code : 123456'}
                    id="standard-basic" label="Code"
                    value={code}
                    onChange={(e) => setCode(e.target.value)}
                />}

                <Box mt={'20px'} justifyContent={'center'} display={'flex'} flexDirection={'column'}>

                    <Button disabled={loading} variant={'contained'} color={'primary'} onClick={(e)=> showCodeField ?  onVerifyCodeSubmit(e) : handleRegister(e)} >
                        {loading && <CircularProgress color={'secondary'} size={21} />}
                        S'inscrire
                    </Button>

                </Box>
                <Box mt={'5px'} display={'flex'} justifyContent={'flex-end'}><Link onClick={() => history.push('/firebase-auth')} > Retourner a l'acceuil</Link></Box>

            </form>



        </Container >


    );
}


export default App;




