import React, { useState } from 'react';
import { Container, Box, TextField, Divider, Button, Grid, CircularProgress, LinearProgress, makeStyles, Link, } from '@material-ui/core';
import styles from './app.module.css'; // This uses CSS modules.
import clsx from 'clsx';
import firebaseConfig from '../../firebaseConfig';
import { Alert } from '@material-ui/lab';
import { useHistory } from 'react-router-dom';


const useStyles = makeStyles((theme) => ({
    root: {
        width: '25%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        maxWidth: '100%',
        height: '100vh',
        backgroundColor: 'transparent',
        marginTop: '20px'
    },
    form: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%',


    },
    logo: { fontfamily: "'Amaranth', sans-serif", fontSize: "200%" },
    logoIcon: {
        top: "4px",
        fontSize: "32px",
        marginRight: "-2px",
        position: "relative",
        color: 'black'


    },
    caption: {
        margin: "20px 0 40px 0",
        fontFamily: "'Amaranth', sans-serif",
        fontSize: "18px",
        opacity: 0.8,
        color: 'black'
    }
}));


const { firebase } = firebaseConfig;

function App() {
    const classes = useStyles();
    const [email, setEmail] = React.useState('')
    const [loading, setLoading] = React.useState(false)
    const [alert, setAlert] = React.useState();
    const history = useHistory();

    const forgotPassword = () => {
        setLoading(true)
        firebase.auth().sendPasswordResetEmail(email)
            .then(function () {
                setAlert({
                    severity:'success',
                    message:'Verifier votre adresse email'
                })
            }).catch(function (e) {
            console.log(e)
        }).finally(()=>setLoading(false))
    }


    return (

        <Container maxWidth={'sm'} className={clsx(classes.root)}  >
            {alert && <Alert severity={alert.severity}>{alert.message}</Alert>}
            <div className={classes.logo}>
                <i className={classes.logoIcon + ' material-icons'}></i> My Snack Flow
            </div>
            <div className={classes.caption}>Tous vos snack en un seul endroit</div>
            <form noValidate autoComplete="off" className={classes.form} >


                <TextField
                    fullWidth
                    id="standard-basic"
                    label="Email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                />


                <Box mt={'20px'} justifyContent={'center'} display={'flex'} flexDirection={'column'}>

                    <Button disabled={loading} variant={'contained'} color={'primary'} onClick={forgotPassword} >
                        {loading && <CircularProgress color={'secondary'} size={21} />}
                        S'inscrire
                    </Button>

                </Box>
                <Box mt={'5px'} display={'flex'} justifyContent={'flex-end'}><Link onClick={() => history.push('/firebase-auth')} > A tu déja un compte ? Se connecter</Link></Box>

            </form>



        </Container >


    );
}


export default App;




