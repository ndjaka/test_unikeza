import React, { Component } from 'react'
import * as firebaseui from 'firebaseui'
import firebaseConfig from '../../firebaseConfig';
// Styles
import styles from './app.module.css'; // This uses CSS modules.

import FirebaseAuth from 'react-firebaseui/FirebaseAuth';
import { Box, Link } from '@material-ui/core';
import { useHistory } from 'react-router-dom';


const { firebase } = firebaseConfig;
const uiConfig = {
  signInFlow: 'popup',
  signInOptions: [
    firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    firebase.auth.EmailAuthProvider.PROVIDER_ID,
  ],
  callbacks: {
    signInSuccessWithAuthResult: () => false,
  },
};

export default function Auth() {

  const history = useHistory();


  React.useEffect(() => {
    const unregisterAuthObserver = firebase.auth().onAuthStateChanged(user => {
      if (user) {
        history.push('/snack-bar')
      }
    });
    return () => unregisterAuthObserver(); // Make sure we un-register Firebase observers when the component unmounts.
  }, []);


  return (
    <div className={styles.container}>
      <div className={styles.logo}>
       My snack Flow
      </div>
      <div className={styles.caption}>Tous vos snacks en un seul endroit</div>

        <div>
          <FirebaseAuth className={styles.firebaseUi} uiConfig={uiConfig}
            firebaseAuth={firebase.auth()} />
        </div>


      <Box mt={'10px'} className={styles.pointer} display={'flex'} justifyContent={'center'} onClick={() => history.push('/firebase-auth')}>Essayez Firebase Auth</Box>

    </div>



  )

}
